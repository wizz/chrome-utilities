#!/usr/bin/env python
# -*- coding: utf-8 -*-

import base64
import binascii
import csv
import logging
import os
import sqlite3
import sys

from logging.config import dictConfig

from Crypto.Cipher import AES
from hashlib import pbkdf2_hmac, sha1

import keyring

CHROME_DATABASE_DEFAULT_MAC = os.path.join(
    os.getenv('HOME', '~/'),
    'Library',
    'Application Support',
    'Google',
    'Chrome',
    'Default',
    'Login Data'
)
CHROMIUM_DATABASE_DEFAULT_MAC = os.path.join(
    os.getenv('HOME', '~/'),
    'Library',
    'Application Support',
    'Chromium',
    'Default',
    'Login Data'
)

PBKDF2_ROUNDS = 1003
PBKDF2_BITS = 128
PBKDF2_SALT = 'saltysalt'
PBKDF2_HASH_FUNC = 'sha1'

AES_MODE = AES.MODE_CBC
AES_IV = '\x20' * 16

DEFAULT_STOREKEY = 'EMPTY'

DEFAULT_LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
}
dictConfig(DEFAULT_LOGGING)
logger = logging.getLogger()


def sqlite3_open(file):
    """
    Opens a Sqlite3 storage database file.
    :param file:    The name of the file
    :returns:       A connection to the database
    """
    return sqlite3.connect(
        file,
        detect_types=sqlite3.PARSE_DECLTYPES
    )


def sqlite3_close(conn):
    """
    Closes an open connection.
    """
    try:
        conn.close()
    except:
        try:
            conn.close()
        except:
            return False
    return True


def sqlite3_cursor(conn, factory=sqlite3.Row):
    """
    Obtains a new valid cursor.
    :param conn:    An already open connection.
    :param factory: A factory which produces result Row objects.
    """
    try:
        if conn.row_factory != factory:
            conn.row_factory = factory
        return conn.cursor()
    except Exception as e:
        raise e


def sqlite3_execute(cursor, statement):
    """
    Executes a statement and returns its results or None no such result exists
    """
    res = None
    try:
        return cursor.execute(statement)
    except Exception as e:
        raise e


def writecsv(data, filename='passwords.csv'):
    """
    Opens a CSV file, write data to it, closes CSV file with flush.
    :param data:
    :param file:
    """
    f = open(filename, 'wb')
    csvwriter = csv.writer(
        f, delimiter=' ',
        quotechar='|',
        quoting=csv.QUOTE_MINIMAL
    )
    for url, username, password in data:
        csvwriter.writerow([url, username, password])
    f.close()


def decrypt(storekey,
            encrypted,
            mode=AES_MODE,
            iv=AES_IV,
            salt=PBKDF2_SALT,
            rounds=PBKDF2_ROUNDS,
            keybits=PBKDF2_BITS,
            hash_func=PBKDF2_HASH_FUNC):
    """
    Derivates the decryption key and decrypts an encrypted string.
    :param storekey:
    :param encrypted:
    :param mode:
    :param iv:
    :param salt:
    :param rounds:
    :param keybits:
    :param hash_func
    """
    key = pbkdf2_hmac(
        hash_func,
        storekey,
        salt,
        rounds,
        keybits
    )

    return AES.new(
        key[:16],
        mode=mode,
        IV=iv
    ).decrypt(encrypted)


def run(database, storekey):
    """
    Runs the tool
    :param database:
    :param storekey:
    """
    conn = sqlite3_open(database)

    curs = sqlite3_cursor(conn)
    res = sqlite3_execute(curs, 'select * from logins')

    result = []
    if res:
        data = curs.fetchone()
        while data:
            try:
                url = str(data['origin_url'])
                username = data['username_value']
                encrypted = data['password_value']

                if encrypted[:3] == 'v10':
                    decrypted = decrypt(
                        storekey, encrypted[3:], AES_MODE, AES_IV)
                else:
                    decrypted = encrypted

                decrypted = decrypted.strip()

                if url is None and username is None and decrypted == encrypted:
                    break

                result += [(url, username, decrypted)]

                logger.debug('-' * 50)
                logger.debug('URL:      %s' % str(url))
                logger.debug('USERNAME: %s', str(username))
                logger.debug('PASSWORD: %s', str(decrypted))

            except:
                logger.exception('Something happend here')

            finally:
                data = curs.fetchone()
        writecsv(result)
    sqlite3_close(conn)


if __name__ == '__main__':
    """
    Maine Programme
    """
    database = CHROME_DATABASE_DEFAULT_MAC
    if len(sys.argv) > 1 and len(sys.argv[1]) > 0:
        database = sys.argv[1]
        if database == 'Chrome':
            database = CHROME_DATABASE_DEFAULT_MAC
        elif database == 'Chromium':
            database = CHROMIUM_DATABASE_DEFAULT_MAC
    else:
        database = CHROME_DATABASE_DEFAULT_MAC

    storekey = DEFAULT_STOREKEY
    if len(sys.argv) > 2 and len(sys.argv[2]) > 0:
        if sys.argv[2][-2:] == '==':
            storekey = sys.argv[2]
        else:
            storekey = keyring.get_password('', sys.argv[2]).encode('utf-8')
    else:
        storekey = keyring.get_password('', 'Chrome').encode('utf-8')

    run(database, storekey)
